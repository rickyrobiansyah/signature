package id.co.rrweypay.samplesign.libsignature;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.WindowManager;


/**
 * Created by ricky on 13/05/16.
 */
public abstract class ActivityMainCore extends ActivityPermission {

    /**
     * The M shared preferences.
     */
    protected SharedPreferences mSharedPreferences;
    /**
     * The M editor.
     */
    protected SharedPreferences.Editor mEditor;


    /**
     * The Class name.
     */
    protected String className = "";
    /**
     * The constant PARAMS_BACK_CLASS.
     */
    public static String PARAMS_BACK_CLASS = "className";
    /**
     * The constant PARAMS_BACK_FORM.
     */
    public static String PARAMS_BACK_FORM = "className";
    /**
     * The M broadcast receiver.
     */
    public BroadcastReceiver mBroadcastReceiver;


    @Override
    public RuntimePermissionHelper setPermissionHelper() {
        RuntimePermissionHelper permissionHelper = new RuntimePermissionHelper(this) {
            @Override
            public Dialog initPermissionDetailDialog(@NonNull String permission, @NonNull String message, int requestCode, int styleRes) {
                Dialog dialog = super.initPermissionDetailDialog(permission, message, requestCode, R.style.WeyPayTheme_Dialog_Alert);
                dialog.setTitle(R.string.permission_request_title);

                return dialog;
            }
        };
        permissionHelper.setDetailDialogPositiveButton(R.string.show);

        // Set the required permissions for this app,
        // these should be the same as the ones defined in Manifest
        permissionHelper = new RuntimePermissionHelper(this);

        // Set the required permissions for this app,
        // these should be the same as the ones defined in Manifest
        setPermissionDialog(permissionHelper);

        return permissionHelper;
    }

    /**
     * Sets permission dialog.
     *
     * @param permissionHelper the permission helper
     */
    protected void setPermissionDialog(RuntimePermissionHelper permissionHelper) {
        permissionHelper.setPermissions(
                new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                },
                new String[]{
                        getResources().getString(R.string.permission_description_write_access_storage),
                        getResources().getString(R.string.permission_description_read_access_storage)
                }
        );
    }

    /**
     * Init view.
     */
    protected abstract void initView();

    /**
     * Init data.
     */
    protected abstract void initData();


    @Override
    protected void onResume() {
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        progressDismiss();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        progressDismiss();
    }

    @Override
    public void onBackPressed() {
        if (className != null && !className.isEmpty()) {
            try {
                Intent intent = new Intent();
                Log.d("getPackageName()", className);
                intent.setComponent(new ComponentName(getPackageName(), className));
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.layout_fade_in_from_left,
                        R.anim.layout_fade_out_to_right);
                this.finish();
            } catch (Exception exception) {
//                backDashboard();
                exception.printStackTrace();
            }
        } else {
//            super.onBackPressed();
            superParentOnBackPress();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initLayoutRes());
        Bundle extra = getIntent().getExtras();
        setFromExtra(extra);
        initOnCreate();
    }


    /**
     * Sets from extra.
     *
     * @param extra the extra
     */
    protected void setFromExtra(Bundle extra) {
        if (extra != null) {
            className = extra.getString(PARAMS_BACK_CLASS, "");
        }
    }

    /**
     * Init on create.
     */
    protected void initOnCreate() {
        initView();
        initData();
    }

    /**
     * Super parent on back press.
     */
    public abstract void superParentOnBackPress();


    /**
     * Get the layout resource ID for the current activity.
     *
     * @return Layout resource ID.
     */
    protected abstract int initLayoutRes();


}
