package id.co.rrweypay.samplesign.libsignature;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Environment;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import me.panavtec.drawableview.DrawableView;
import me.panavtec.drawableview.DrawableViewConfig;

public class InputSignatureActivity extends ActivityMainCore {


    public static final String DATA_RESULT = "PATH_IMAGE_RESULT";
    public static final String NAME_FILE = "NAME_FILE";
    public static final String SET_TITLE = "TITLE";
    private ImageView imgBack;
    private ImageView imgClear;
    private ImageView imgUndo;
    private ImageView imgSave;
    private TextView tvTitle;
    private DrawableView drawableView;

    private String pathImageResult = "";
    private String imageFile = "";

    @Override
    protected void initView() {

        drawableView = (DrawableView) findViewById(R.id.paintView);
        imgBack = (ImageView) findViewById(R.id.btn_back);
        imgUndo = (ImageView) findViewById(R.id.btn_undo);
        imgSave = (ImageView) findViewById(R.id.btn_save);
        imgClear = (ImageView) findViewById(R.id.btn_clear);
        tvTitle = (TextView) findViewById(R.id.txt_title);

        Bundle extra = getIntent().getExtras();
        String title = "";
        if (extra != null) {
            title = extra.getString(SET_TITLE, "");
        }
        if (!title.isEmpty() && !title.equals("")) {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        onFinish();
    }

    @Override
    protected void initData() {
        DrawableViewConfig config = new DrawableViewConfig();
        config.setStrokeColor(getResources().getColor(android.R.color.black));
        config.setShowCanvasBounds(true); // If the view is bigger than canvas, with this the user will see the bounds (Recommended)
        config.setStrokeWidth(15.0f);
        config.setMinZoom(1.0f);
        config.setMaxZoom(1.5f);
        config.setCanvasHeight(1080);
        config.setCanvasWidth(1920);
        drawableView.setConfig(config);
        drawableView.setBackgroundResource(R.color.colorAccent);


        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFinish();
            }
        });
        imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawableView.clear();
            }
        });
        imgUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawableView.undo();
            }
        });
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Assume block needs to be inside a Try/Catch block.
                saveImage();
                onFinish();
            }
        });
    }

    @Override
    public void superParentOnBackPress() {
        onFinish();
    }

    @Override
    protected int initLayoutRes() {
        return R.layout.act_lib_signature;
    }

    private void onFinish() {
        Intent data = new Intent();
        data.putExtra(DATA_RESULT, pathImageResult);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        this.finish();
    }

    private final void saveImage() {
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        Integer counter = 0;
        File file = new File(path, getString(R.string.app_name) + new Date().getTime() + ".png"); // the File to save , append increasing numeric counter to prevent files from getting overwritten.
        try {
            fOut = new FileOutputStream(file);
            Bitmap pictureBitmap = drawableView.obtainBitmap(); // obtaining the Bitmap
            pictureBitmap.setHasAlpha(true);
            Canvas canvas = new Canvas(pictureBitmap);
            pathImageResult = file.getAbsolutePath();
//                    canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(pictureBitmap, 0, 0, null);
//                    img.setImageBitmap(pictureBitmap);
            if (pictureBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut)) {// saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                Log.d("tags", "Success");
            } else {
                Log.d("tags", "Gagal");
            }
            fOut.flush(); // Not really required
            fOut.close(); // do not forget to close the stream
//                    MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(), file.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file.exists()) {
            Log.d(file.getName(), file.getAbsolutePath());
            Log.d("exist", "Success");
        }
    }


}
