package id.co.rrweypay.signature.atozricky2;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import id.co.rrweypay.samplesign.libsignature.ActivityMainCore;
import id.co.rrweypay.samplesign.libsignature.InputSignatureActivity;

public class SampleTestAMC extends ActivityMainCore {

    private ImageView imgResult;
    private TextView btnSignature;


    @Override
    protected void initView() {
        imgResult = (ImageView) findViewById(R.id.image_result);
        btnSignature = (TextView) findViewById(R.id.btn_getsignature);
    }

    @Override
    protected void initData() {
        btnSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashboard = new Intent(SampleTestAMC.this, InputSignatureActivity.class);
                String title = "Your Signature";
                dashboard.putExtra(InputSignatureActivity.SET_TITLE, title);
                dashboard.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivityForResult(dashboard, 99);
                overridePendingTransition(R.anim.layout_fade_in_from_right,
                        R.anim.layout_fade_out_to_left);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 99) {
            String tempFile = data.getExtras().getString(InputSignatureActivity.DATA_RESULT, "");
            Log.d("tempFile", tempFile);
            if (!tempFile.isEmpty() && !tempFile.equals("")) {
                Glide.with(this).load(tempFile).into(imgResult);
            }
        }
    }

    @Override
    public void superParentOnBackPress() {
        finish();
    }

    @Override
    protected int initLayoutRes() {
        return R.layout.activity_sample;
    }
}
